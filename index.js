// Syntax:
    // fetch('url', options)
        // url - this is the url which the request is to be made
        // options - an array of properties, optional parameter


// Show posts
fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then(data => showPosts(data))

// Show posts function

const showPosts = (posts) => {

    let postEntries = "";

    posts.forEach(post => {
        console.log(post);

        postEntries += `
            <div id="post-${post.id}">

                <h3 id="post-title-${post.id}">${post.title}</h3>
                
                <p id="post-body-${post.id}">${post.body}</p>

                <button onclick="editPost('${post.id}')">Edit</button>

                <button onclick="deletePost('${post.id}')">Delete</button>

            </div>
        `;
    })

    document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// Add posts

document.querySelector("#form-add-post").addEventListener("submit", (e) => {

    e.preventDefault();
    
    fetch('https://jsonplaceholder.typicode.com/posts', {

        method: 'POST',
        body: JSON.stringify({
            title: document.querySelector("#txt-title").value,
            body: document.querySelector("#txt-body").value,
            userId: 1
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json()) // transform response to json
    .then(data => {
        console.log(data);
        alert('Successfully added');

        // querySelector = null, resets the state of our input into blanks after submitting a new post
        document.querySelector("#txt-title").value = null;
        document.querySelector("#txt-body").value = null;
    })
})

// Edit post

const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value= id;
    document.querySelector('#txt-edit-title').value= title;
    document.querySelector('#txt-edit-body').value= body;

    document.querySelector("#btn-submit-update").removeAttribute('disabled');
};

// update post

document.querySelector("#form-edit-post").addEventListener('submit', (e) => {

    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts/1', {

        method: "PUT",
        body: JSON.stringify({
            id: document.querySelector("#txt-edit-id").value,
            title: document.querySelector("#txt-edit-title").value,
            body: document.querySelector("#txt-edit-body").value,
            userId: 1
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(data => {
        console.log(data)
        alert('Post successfully updated');

        document.querySelector('#txt-edit-id').value = null;
        document.querySelector('#txt-edit-title').value = null;
        document.querySelector('#txt-edit-body').value = null;

        document.querySelector('#btn-submit-update').setAttribute('disabled', true);
    })
})

// delete post

const deletePost = (id) => {

    fetch('https://jsonplaceholder.typicode.com/posts/1', {

        method: "DELETE",
        body: JSON.stringify({
            id: document.querySelector(`#post-${id}`).value,
            title: document.querySelector(`#post-title-${id}`).value,
            body: document.querySelector(`#post-body-${id}`).value,
            userId: 1
        }),
        headers: {
            'Content-Type': 'application/json'
        }

    })
    .then(response => response.json())
    .then(data => {

        document.querySelector(`#post-${id}`).remove();
        document.querySelector(`#post-title-${id}`).remove();
        document.querySelector(`#post-body-${id}`).remove();
        
    })
}